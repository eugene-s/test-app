# Coinmena Test

## Running locally

### Requirements

- Docker

### Commands

```bash
docker-compose pull
docker-compose build
docker-compose up -d
```

### Cleanup

```bash
docker-compose down
docker volume rm coinmena-test_mediafiles coinmena-test_postgres coinmena-test_rabbitmq coinmena-test_staticfiles
```

### Use case

1. Start the project
2. Open [http://localhost:8080/admin](http://localhost:8080/admin) and login with `root:root`
3. Create a token in admin
4. Execute Curl commands:
    ```bash
    # force to refresh a rate
    curl -X POST http://localhost:8080/api/v1/quotes -H 'Authorization: Token <your token>'
    # get the latest rate
    curl -X GET http://localhost:8080/api/v1/quotes -H 'Authorization: Token <your token>'
    ```
