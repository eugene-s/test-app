from decimal import Decimal
from unittest.mock import patch

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.test.utils import override_settings
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from rest_framework import status
from .models import Currency, QuoteExchangeRate
from alpha_vantage.cryptocurrencies import CryptoCurrencies
from . import serializers, tasks


User = get_user_model()

MOCK_EXCHANGE_RATE_1 = (
    {
        "1. From_Currency Code": "BTC",
        "2. From_Currency Name": "Bitcoin",
        "3. To_Currency Code": "USD",
        "4. To_Currency Name": "United States Dollar",
        "5. Exchange Rate": "66744.17000000",
        "6. Last Refreshed": "2021-11-10 11:43:01",
        "7. Time Zone": "UTC",
        "8. Bid Price": "66744.16000000",
        "9. Ask Price": "66744.17000000",
    },
    None,
)

MOCK_EXCHANGE_RATE_2 = (
    {
        "1. From_Currency Code": "BTC",
        "2. From_Currency Name": "Bitcoin",
        "3. To_Currency Code": "USD",
        "4. To_Currency Name": "United States Dollar",
        "5. Exchange Rate": "66644.17000000",
        "6. Last Refreshed": "2021-11-10 11:43:01",
        "7. Time Zone": "UTC",
        "8. Bid Price": "66744.16000000",
        "9. Ask Price": "66744.17000000",
    },
    None,
)


@override_settings(CELERY_TASK_ALWAYS_EAGER=True)
class QuoteExchangeRateViewSetTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="test", email="test@test.com")
        self.token = Token.objects.create(user=self.user).key
        self.client = APIClient()

    def test_get_post(self):
        # Unauthorized
        response = self.client.get("/api/v1/quotes", format="json")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # Authorized
        self.client.credentials(HTTP_AUTHORIZATION=f"Token {self.token}")
        response = self.client.get("/api/v1/quotes", format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data, {"from_currency": None, "to_currency": None, "rate": None}
        )

        # object exists
        with patch.object(
            CryptoCurrencies,
            "get_digital_currency_exchange_rate",
        ) as mock:
            mock.return_value = MOCK_EXCHANGE_RATE_1
            response = self.client.post("/api/v1/quotes", format="json")
            self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
            self.assertEqual(response.data, {})
            self.assertEqual(QuoteExchangeRate.objects.count(), 1)
        rate = QuoteExchangeRate.objects.first()
        rate_serialized = serializers.QuoteExchangeRate(rate)
        response = self.client.options("/api/v1/quotes", format="json")
        response = self.client.get("/api/v1/quotes", format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            {
                "from_currency": "BTC",
                "to_currency": "USD",
                "rate": "66744.1700000000",
                "created_at": rate_serialized.data["created_at"],
            },
        )


MOCK_EXCHANGE_RATE_1 = (
    {
        "1. From_Currency Code": "BTC",
        "2. From_Currency Name": "Bitcoin",
        "3. To_Currency Code": "USD",
        "4. To_Currency Name": "United States Dollar",
        "5. Exchange Rate": "66744.17000000",
        "6. Last Refreshed": "2021-11-10 11:43:01",
        "7. Time Zone": "UTC",
        "8. Bid Price": "66744.16000000",
        "9. Ask Price": "66744.17000000",
    },
    None,
)

MOCK_EXCHANGE_RATE_2 = (
    {
        "1. From_Currency Code": "BTC",
        "2. From_Currency Name": "Bitcoin",
        "3. To_Currency Code": "USD",
        "4. To_Currency Name": "United States Dollar",
        "5. Exchange Rate": "66644.17000000",
        "6. Last Refreshed": "2021-11-10 11:43:01",
        "7. Time Zone": "UTC",
        "8. Bid Price": "66744.16000000",
        "9. Ask Price": "66744.17000000",
    },
    None,
)


class RefreshAlphavantageExrateTestCase(TestCase):
    def test_refresh_alphavantage_exrate(self):
        with patch.object(
            CryptoCurrencies,
            "get_digital_currency_exchange_rate",
        ) as mock:
            mock.return_value = MOCK_EXCHANGE_RATE_1
            tasks.refresh_alphavantage_exrate(Currency.BTC, Currency.USD)
            mock.return_value = MOCK_EXCHANGE_RATE_2
            tasks.refresh_alphavantage_exrate(Currency.BTC, Currency.USD)
            rate = QuoteExchangeRate.objects.first()
            self.assertIsNotNone(rate)
            self.assertEqual(
                rate.rate, Decimal(MOCK_EXCHANGE_RATE_2[0]["5. Exchange Rate"])
            )
            self.assertEqual(QuoteExchangeRate.objects.count(), 2)
