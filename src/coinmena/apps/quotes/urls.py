from .routers import QuotesRouter
from . import views

router = QuotesRouter()
router.register(r"quotes", views.QuoteExchangeRate)

urlpatterns = router.urls
