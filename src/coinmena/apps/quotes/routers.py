from rest_framework.routers import Route, SimpleRouter


class QuotesRouter(SimpleRouter):
    routes = [
        Route(
            url=r"^{prefix}$",
            mapping={"get": "retrieve", "post": "create"},
            name="{basename}-retrieve",
            detail=True,
            initkwargs={"suffix": "Detail"},
        ),
    ]
