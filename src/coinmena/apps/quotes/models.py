from django.db import models
from django.utils.translation import ugettext_lazy as _


class Currency(models.TextChoices):
    BTC = "BTC"
    USD = "USD"


class QuoteExchangeRate(models.Model):
    from_currency = models.CharField(choices=Currency.choices, max_length=3)
    to_currency = models.CharField(choices=Currency.choices, max_length=3)
    rate = models.DecimalField(max_digits=19, decimal_places=10)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-created_at",)
