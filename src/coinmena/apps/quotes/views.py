from rest_framework import status
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response

from . import models, serializers, tasks
from .models import Currency


class QuoteExchangeRate(RetrieveModelMixin, GenericViewSet):
    queryset = models.QuoteExchangeRate.objects.get_queryset()
    serializer_class = serializers.QuoteExchangeRate

    def get_serializer_class(self):
        if self.action == "create":
            return serializers.QuoteExchangeRateEmpty
        return super().get_serializer_class()

    def get_queryset(self):
        queryset = super().get_queryset()

        # bind this viewset only to BTC/USD exchange rate
        queryset = queryset.filter(
            from_currency=Currency.BTC,
            to_currency=Currency.USD,
        )

        return queryset

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.first()

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def create(self, request, *args, **kwargs):
        # todo: make this call as singleton. Need to think about...
        tasks.refresh_alphavantage_exrate.delay(Currency.BTC, Currency.USD)
        return Response({}, status=status.HTTP_202_ACCEPTED)


__all__ = "QuoteExchangeRate",
