from rest_framework.serializers import ModelSerializer
from . import models


class QuoteExchangeRateEmpty(ModelSerializer):
    class Meta:
        model = models.QuoteExchangeRate
        fields = ()


class QuoteExchangeRate(ModelSerializer):
    class Meta:
        model = models.QuoteExchangeRate
        fields = "from_currency", "to_currency", "rate", "created_at"


__all__ = "QuoteExchangeRate", "QuoteExchangeRateEmpty"
