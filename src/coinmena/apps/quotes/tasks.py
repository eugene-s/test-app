import os
from decimal import Decimal

from alpha_vantage.cryptocurrencies import CryptoCurrencies

from coinmena import celery_app
from .models import QuoteExchangeRate

task = celery_app.task

ALPHAVANTAGE_API_KEY = os.getenv("ALPHAVANTAGE_API_KEY", None)


@task
def refresh_alphavantage_exrate(from_currency, to_currency):
    """
    Getting the latest exchange rate of {from_currency}/{to_currency}

    todo: Better to separate this func for two steps,
    fetching data from API and storing data to DB.
    But at this moment it will be enough.
    """
    cc = CryptoCurrencies(key=ALPHAVANTAGE_API_KEY)
    res, _ = cc.get_digital_currency_exchange_rate(
        from_currency=from_currency, to_currency=to_currency
    )
    QuoteExchangeRate.objects.create(
        from_currency=from_currency,
        to_currency=to_currency,
        rate=Decimal(res["5. Exchange Rate"]),
    )
