from django.contrib import admin
from . import models


@admin.register(models.QuoteExchangeRate)
class QuoteExchangeRateAdmin(admin.ModelAdmin):
    pass
