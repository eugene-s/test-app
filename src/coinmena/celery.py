import os

from celery import Celery

# Set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "coinmena.settings")

app = Celery("coinmena")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    "periodic-refresh-btc-usd-exrate-every-1-hour": {
        "task": "coinmena.apps.quotes.tasks.refresh_alphavantage_exrate",
        "schedule": 3600.0,
        "args": ("BTC", "USD"),
    },
}
app.conf.timezone = "UTC"
