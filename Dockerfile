FROM python:3.9-slim

RUN apt-get update && apt-get install \
    tini \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /

RUN pip install --no-cache -r requirements.txt

WORKDIR /app

COPY src .

ENTRYPOINT ["/usr/bin/tini", "--"]
